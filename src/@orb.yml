version: 2.1

description: |
  Ensure a terraform module is following best practices before deploying to an S3 Registry.
  View this orb's source: https://github.com/Brightspace/terraform-circleci-orbs

executors:
  awscli:
    description: |
      A debian-based docker container to use when installing/configuring the AWS CLI
    parameters:
      version:
        type: string
        default: "3.7.0"
      debian:
        type: string
        default: "stretch-browsers"
    docker:
      - image: circleci/python:<<parameters.version>>-<<parameters.debian>>

  terraform:
    description: |
      A alpine-based docker container to use for terraform and tflint
    parameters:
      version:
        type: string
        default: "0.12.12"
    docker:
      - image: hashicorp/terraform:<<parameters.version>>

jobs:
  providers:
    description: |
      Retrieves third party providers from an AWS S3 store based on a JSON key-value file.
    executor: awscli
    parameters:
      bucket:
        description: |
          S3Uri for the bucket that stores third party providers. Expected to be in the format `s3://my-terraform-registry`.
        type: string
      prefix:
        description: |
          S3 prefix that stores the third party providers. Expected to be in the format `/my/path/here`.
        type: string
      filename:
        description: |
          "Path to the JSON key-value pair file declaring the third party providers. Defaults to the path `package.json`. 

          Expected to be in the format `{ 'terraform-provider-test': '1.0.0' }`"
        type: string
        default: package.json
      packed-path:
        description: |
          Path to directory that will store the third party providers. This directory will be persisted in the workspace.

          This directory follows the format of the user plugins directory.
          https://www.terraform.io/docs/configuration/providers.html#third-party-plugins
        type: string
        default: providers
    steps:
      - checkout
      - awscli
      - run:
          name: Download custom providers from AWS S3
          command: |
            GOOS="linux"
            GOARCH="amd64"
            s3bucket="<<parameters.bucket>><<parameters.prefix>>"

            mkdir -p "<<parameters.packed-path>>/plugins/"
            touch "<<parameters.packed-path>>/plugins/terraform"
            find . -type f -iname "<<parameters.filename>>" -print0 | while IFS= read -r -d $'\0' line; do
                echo "$line"
                jq -r 'keys[]' "$line"
                for provider in $(jq -r 'keys[]' "$line"); do
                    ver="$(jq -r ".\"${provider}\"" "$line")"
                    echo "${provider}:${ver}"
                    file="${provider}_${ver}_${GOOS}_${GOARCH}.zip"
                    bucket="${s3bucket}/${provider}/${ver}/${file}"

                    mkdir -p <<parameters.packed-path>>/vendor/ <<parameters.packed-path>>/plugins/${GOOS}_${GOARCH}/
                    aws s3 cp --no-progress --quiet "${bucket}" <<parameters.packed-path>>/vendor/
                    unzip -oj "<<parameters.packed-path>>/vendor/${file}" "${provider}_v${ver}" -d "<<parameters.packed-path>>/plugins/${GOOS}_${GOARCH}"
                done
            done
      - run:
          name: List contents of the directory
          command: find <<parameters.packed-path>>/ -print | sed -e 's;/*/;|;g;s;|; |;g'
      - persist_to_workspace:
          root: .
          paths:
            - "<<parameters.packed-path>>/**/*"
  lint:
    description: |
      Validates the configuration files in a directory, ensuring that the configuration
      is syntactically valid and internally consistent.
    executor: terraform
    parameters:
      path:
        description: |
          Path to the terraform module. Defaults to repository root.
        type: string
        default: .
      usage-path:
        description: |
          Path to the terraform usage module. A usage module determines how
          a terraform module is deployed across multiple accounts.

          If not specified, no linting will be performed.
        type: string
        default: ""
      provider-path:
        description: |
          Location of third-party providers that will be copied into the user plugins directory.
        type: string
        default: "providers"
      environment-path:
        description: |
          Path to an environment file to be loaded during linting.
        type: string
        default: ".env"
    steps:
      - checkout
      - attach_workspace:
          at: ./
      - lint:
          path: <<parameters.path>>
          provider-path: <<parameters.provider-path>>
          environment-path: <<parameters.environment-path>>
      - when:
          condition: <<parameters.usage-path>>
          steps:
            - lint:
                path: <<parameters.usage-path>>
                provider-path: <<parameters.provider-path>>
                environment-path: <<parameters.environment-path>>
