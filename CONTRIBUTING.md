# Contributing

The following is a set of guidelines for contributing to this CircleCI Orb. These are mostly guidelines, not rules. Use your best judgment, and feel free to propose changes to this document in a pull request.

## Did you find a bug? Or want to propose a change?

If you discovered an issue with any of the resources (jobs, executors, commands), you can open up an issue on this repository, using one of the issue templates as a starting point. The issue templates should give you everything you need to get started.

## Contributing

### Ensuring your changes work

For any proposed change you should make should it works with a minimum CircleCI build. You can learn more about this from the [Orb Starter Kit](https://github.com/CircleCI-Public/orb-starter-kit), or the [documentation](https://circleci.com/docs/2.0/using-orbs). After a pull request has been reviewed, the changes will be tested with a preview build. When that has been verified to work, the pull request can be merged.

### Orb Conventions

Start reading our code and you'll get the hang of it. We optimize for simplicity:

  * Try to ensure the yml files are formatted (prettier works well!)
  * We use simple-as-possible variable names (`bucket` over `aws_s3_bucket_uri`)
  * Try to split into commands if using standard commands (`terraform XYZ`)
  * Ensure descriptions exist on all parameters + executors/jobs/commands
