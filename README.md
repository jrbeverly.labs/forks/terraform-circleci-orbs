# Terraform Orbs [![CircleCI](https://circleci.com/gh/Brightspace/terraform-circleci-orbs.svg?style=svg)](https://circleci.com/gh/Brightspace/terraform-circleci-orbs) [![CircleCI Community](https://img.shields.io/badge/community-CircleCI%20Discuss-343434.svg)](https://discuss.circleci.com/c/ecosystem/orbs)

A terraform orb for assisting with linting and deploying terraform modules.

## Usage

**This orb is working off jrbeverly/terraform until permissions can be sorted**

You can use the orb in your workflows like so:

```yml
version: 2.1

orbs:
  terraform: brightspace/terraform@0.0.12

workflows:
  build:
    jobs:
      # Download third party providers from S3
      - terraform/providers:
          # Download from s3://my-registry/providers
          bucket: s3://my-registry
          prefix: /providers
          filename: key-value-pair.json
      
      # Lint the terraform
      - terraform/lint:
          # Module is at root of repository
          path: .

          # Load in dummy environment variables for validation
          environment-path: ".env.sample"
          requires:
            - terraform/providers
```

For the full list of available jobs, commands and executors, see the [orb/brightspace/terraform](https://circleci.com/orbs/registry/orb/brightspace/terraform).

## CircleCI Orbs

Orbs are reusable [commands](https://circleci.com/docs/2.0/reusing-config/#authoring-reusable-commands), [executors](https://circleci.com/docs/2.0/reusing-config/#authoring-reusable-executors), and [jobs](https://circleci.com/docs/2.0/reusing-config/#jobs-defined-in-an-orb) (as well as [usage examples](https://github.com/CircleCI-Public/config-preview-sdk/blob/v2.1/docs/usage-examples.md))—snippets of CircleCI configuration—that can be shared across teams and projects. See [CircleCI Orbs](https://circleci.com/orbs), [Explore Orbs](https://circleci.com/orbs/registry), [Creating Orbs](https://circleci.com/docs/2.0/creating-orbs), and [Using Orbs](https://circleci.com/docs/2.0/using-orbs) for further information.

## Contributing

Please read through our [contributing guidelines](CONTRIBUTING.md). Included are directions for opening issues, coding standards, and notes on development.

## Licensing

Please refer to the [Orb Licensing](https://circleci.com/orbs/registry/licensing) for the licensing of CircleCI Orbs.