---
name: Bug report
about: Create a report to help us improve
title: ''
labels: bug
assignees: ''

---

**Describe the bug**
A clear and concise description of what the bug is.

**Minimal CircleCI Configuration**
A minimal usage of the circleci orb

```yml
version: 2.1

orbs:
  terraform: brightspace/terraform@0.0.12

# ...
```

**Expected behavior**
A clear and concise description of what you expected to happen.

**Logs**
Exerts from the logs that are relevant to the issue. Please remember to remove any secrets/confidential information before posting the logs.

```
...
```

**Additional context**
Add any other context about the problem here.
